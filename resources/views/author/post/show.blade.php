@extends('layouts.backend.app')

@section('title','Post')

@push('css')
   
@endpush

@section('content')
    <div class="container-fluid">

            <div class="row clearfix">
               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ $post->title }}
                                <small>Posted By <strong> <a href="">{{ $post->user->name }}</a></strong> on {{ $post->created_at->toFormattedDateString() }}</small>
                            </h2>
                                <a href="{{route('author.post.index')}}" class="btn btn-success btn-sm waves-effect pull-right">Back To Post List</a>
                               
                                @if($post->is_approved == true)
                                    <span class="label bg-green">Approved</span>
                                @else
                                    <span class="label bg-green">Not Approved</span>
                                @endif

                        </div>
                        <div class="body">
                            {!! $post->body !!}   
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                Categoory
                            </h2>
                        </div>
                        <div class="body">
                            @foreach ($post->categories as $category)
                                <span class="label bg-cyan">{{ $category->name }}</span>
                            @endforeach
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-green">
                            <h2>
                                Tag
                            </h2>
                        </div>
                        <div class="body">
                            @foreach ($post->tags as $tag)
                                <span class="label bg-green">{{ $tag->tag_name }}</span>
                            @endforeach
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-amber">
                            <h2>
                                Featured Image
                            </h2>
                        </div>
                        <div class="body">
                          <img class="img-responsive thumbnail" src="{{ Storage::disk('public')->url('post/'.$post->image) }}" alt="Ok">
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
   
@endpush
